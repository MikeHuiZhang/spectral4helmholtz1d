function u= wavcol(xl,xr,k,fs)
% solve
% - - special solution
splitting on;
g= chebfun(@(x)-1i*exp(1i*k.*abs(x))./(2*k), [xl-xr,xr-xl]); % fund. sol.
f= chebfun(fs,[xl,xr]); % the source function
us= restrict(conv(f,g),[xl,xr]); % a special solution
% figure, plotcfun(us,xl,xr), title('special solution');
% - - solve using boundary data u_n - ik u of the special solution
p1= chebfun(@(x)exp(1i*k.*x),[xl,xr]);
p2= chebfun(@(x)exp(-1i*k.*x),[xl,xr]);
p1x= diff(p1); 
p2x= diff(p2);
A= [-p1x(xl)-1i*k*p1(xl), -p2x(xl)-1i*k*p2(xl)
    p1x(xr)-1i*k*p1(xr), p2x(xr)-1i*k*p2(xr)];
usx= diff(us);
b(2)= usx(xr)-1i*k*us(xr);  b(1)= -usx(xl)-1i*k*us(xl); b= b(:);
c= A\b;
up= c(1)*p1 + c(2)*p2; % plane wave solution
% figure, plotcfun(up,xl,xr),title('plane waves solution');
u= up + us; % the final solution
% figure, plotcfun(u,xl,xr), title('final solution: special + wave');