function [uh,x,A,fh]= chebcol(xl,xr,k2,fs,N)
%solve the Helmholtz problem on an interval
%       u_{xx} + k^2 u = f
% with the first-order absorbing boundary condition
%       u_n - ik u = 0
% by Chebyshev collocation
% N: In
%   number of Chebyshev nodes is N+1

[Dx,x]= chebtointv(xl,xr,N);
Dxx= Dx^2;
mass= k2.*ones(N+1,1);
mass= spdiags(mass(:),0,size(Dxx,1),size(Dxx,2));
A= Dxx + mass;
k= sqrt(k2);
A(1,:)= -Dx(1,:); A(1,1)= A(1,1) - 1i*k;
A(N+1,:)= Dx(N+1,:); A(N+1,N+1)= A(N+1,N+1) - 1i*k;
fh= fs(x); fh([1,N+1])= 0; fh= fh(:);
uh= A\fh;