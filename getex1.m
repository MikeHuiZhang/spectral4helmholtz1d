function [u,f]= getex1(x,k)
%[u,f]= getex1(x,k2)
% gives an example with exact solution u satisfying
%        u_{xx} + k^2 u = f
N= length(x)-1;
u= exp(1i*k.*x);
ux= 1i*k.*u;
f= zeros(N+1,1);
f(1)= -ux(1) - 1i*k*u(1);
f(N+1)= ux(N+1)- 1i*k*u(N+1);

u= u(:);
f= f(:);
