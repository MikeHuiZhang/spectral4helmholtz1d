function [Dx,x]= chebtointv(xl,xr,N)
% compute the differentiation matrix and Chebyshev nodes on [-1,1]
% by the program cheb.m from Treffethen's book 'Spectral methods in Matlab'
% and scale to the interval [xl,xr]
[Dref,xref] = cheb(N); 
xref= flipdim(xref,1); % from -1 to 1
Dref= rot90(Dref,2);
% scale them to our geometry
Dx= Dref*2/(xr-xl); 
x= xl + (xref+1)*(xr-xl)/2;