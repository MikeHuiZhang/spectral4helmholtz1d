% solve the Helmholtz equation on an interval
%       u_{xx} + k^2 u = f
% with the first-order absorbing boundary condition
%       u_n - ik u = 0
% by Chebyshev collocation

% clear all;

% wavenumber
k= 1; k2= k^2;
% geometry
xl= -1; xr= 1;
% source 
xs= xl + (xr-xl)/2;
a= 1/10; 
fs= @(x) (abs(x-xs)<=a).*exp(a^2./(abs(x-xs).^2-a^2));

% solve
N= 100;
[uh,xh]= chebcol(xl,xr,k2,fs,N);
uh= chebfun(uh,[xl,xr]);
u= wavcol(xl,xr,k,fs);
plot(real(uh),'.-'), hold on, plot(real(u),'r.-'), plot(real(uh-u),'g');
legend('cheb solution', 'wave solution','the difference');

% remove plane waves from the solution by L^2 projection of the solution
% u= chebfun(uh,[xl,xr]);
% p1= chebfun(@(x)(cos(k.*x)),[xl,xr]);
% p2= chebfun(@(x)(sin(k.*x)),[xl,xr]);
% v= u;
% v= v-sum(v.*p1)/sum(p1.*p1)*p1;
% v= v-sum(v.*p2)/sum(p2.*p2)*p2;
% hold off; plot((real(u)),'.-'); hold on;
% plot(real(v),'r'); legend('the solution', 'after plane waves removed');

% [uH,xH]= chebcol(xl,xr,80,k2,fs);
% disp('max |uh-uH|');
% xi= linspace(xl,xr,401);
% uhi= interp1(xh,uh,xi,'spline');
% uHi= interp1(xH,uH,xi,'spline');
% disp(max(abs(uhi-uHi)));

