function plotcfun(u,xl,xr)
hold off;
plot(real(u), 'interval', [xl,xr]); 
hold on; 
plot(imag(u), 'interval', [xl,xr],'r'); 
legend('real part','imaginary part'); 